import socket
BUFSIZE = 1024
ip_port = ('0.0.0.0', 9998)
server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server.bind(ip_port)
while True:
        data,client_addr = server.recvfrom(BUFSIZE)
        print('server result', data)
        server.sendto(data.upper(),client_addr)
server.close()
