FROM karthrand/ubuntu:udp 
MAINTAINER jinxiao
ADD server.py /root/server.py
EXPOSE 9998/udp
CMD ["python","/root/server.py"]
